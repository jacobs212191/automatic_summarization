# coding: utf-8
from __future__ import unicode_literals
from io import open
import pickle
import re
import math
from math import sqrt
from math import pi
from math import exp
from math import ceil
from math import floor
import sys
from pathlib import Path
import nltk.data
import copy
import random
import torch
import torch.nn as nn
import numpy as np

root = Path(__file__).absolute().parents[0]

print(str(root))

File_object = open(str(root)+"/Helpers/Resource/stop_words_pickle","rb")
stop_words = pickle.load(File_object)
File_object.close()

File_object = open(str(root)+"/Helpers/dataset_raw/articles_everything_pickled","rb")
data_raw = pickle.load(File_object)
File_object.close()

#File_object = open(str(root)+"/Helpers/Resource/sskj_alphabetcly_organized_pickled","rb")
File_object = open(str(root)+"/Helpers/Resource/sskj_hashmap","rb")
sskj = pickle.load(File_object)
File_object.close()

# Occurence of every word in all of documents document
File_object = open(str(root)+"/Helpers/Resource/occurence_of_every_word_once_in_document_pickled","rb") 
occurence_of_every_word_once_in_document = pickle.load(File_object)
File_object.close()

# bag of words for every sentence of every article
File_object = open(str(root)+"/Helpers/Resource/bag_of_words_all_pickled","rb") 
bag_of_words_all = pickle.load(File_object)
File_object.close()

File_object = open(str(root)+"/Helpers/Resource/vocab_pickle","rb")
vocab = pickle.load(File_object)
File_object.close()


big_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZČŠŽ'
chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZČčŠšŽžÐßäöÖÄđćäåæçëïńößşü 1234567890'
nums = '1234567890'

def create_BOW(string):
	BOW = dict()
	for l in string:
		if l not in BOW:
			BOW[l] = 1
		else:
			BOW[l] += 1
	return BOW

class CBOW(torch.nn.Module):

	def __init__(self, vocab_size, embedding_dim):
		super(CBOW, self).__init__()

		#out: 1 x emdedding_dim
		self.embeddings = nn.Embedding(vocab_size, embedding_dim)

		self.linear1 = nn.Linear(embedding_dim, 128)

		self.activation_function1 = nn.ReLU()
		
		#out: 1 x vocab_size
		self.linear2 = nn.Linear(128, vocab_size)

		self.activation_function2 = nn.LogSoftmax(dim = -1)
		

	def forward(self, inputs):
		embeds = sum(self.embeddings(inputs)).view(1,-1)
		out = self.linear1(embeds)
		out = self.activation_function1(out)
		out = self.linear2(out)
		out = self.activation_function2(out)
		return out

	def get_word_emdedding(self, word):
		word = torch.LongTensor([word_to_ix[word]])
		return self.embeddings(word).view(1,-1)

def make_context_vector(context, word_to_ix):
	idxs = [word_to_ix[w] for w in context]
	return torch.tensor(idxs, dtype=torch.long)

def get_index_of_max(input):
	index = 0
	for i in range(1, len(input)):
		if input[i] > input[index]:
			index = i 
	return index

def get_max_prob_result(input, ix_to_word):
	return ix_to_word[get_index_of_max(input)]

CONTEXT_SIZE = 2  # 2 words to the left, 2 to the right
EMDEDDING_DIM = 100

def get_cosine(vec1, vec2):
	intersection = set(vec1.keys()) & set(vec2.keys())
	numerator = sum([vec1[x] * vec2[x] for x in intersection])

	sum1 = sum([vec1[x]**2 for x in vec1.keys()])
	sum2 = sum([vec2[x]**2 for x in vec2.keys()])
	denominator = math.sqrt(sum1) * math.sqrt(sum2)

	if not denominator:
		return 0.0
	else:
		return float(numerator) / denominator


# ------------------- PARSING -------------------------


def preProcess(text):

	tokenizer = nltk.data.load('tokenizers/punkt/slovene.pickle')
	new_article = []
	
	for paragraph in text:
		for s in tokenizer.tokenize(paragraph):
			if(len(s) > 1):

				string = s.replace(u'\xa0', u'')
				string = string.replace(u'\n', u'')

				end = ''
				if(len(string)>1):
					if(string[-1] not in ['!','.','?']):
						#string = string+'.'
						end = '.'
					else:
						end = string[-1]

				for l in string:
					if(l not in chars):
						string = string.replace(l,' ')

						
						
								

				string = string.split(' ')
				ns = []
				for w in string:
					if(len(w) > 0):
						ns.append(w)

				string = ' '.join(ns)
				string += end
				print(string)

				if(len(string)>1):
					new_article.append(string)
				else:
					pass



	return(new_article)

# ------------------------- Feature extraction ------------------------------------------

def feature_extraction(article):

	articleNew = copy.deepcopy(article)
	nums_in_text = copy.deepcopy(article)
	non_vocab = copy.deepcopy(article)
	vocab_s = copy.deepcopy(article)

	sentence_counter = 0
	for sentence in article:
		new_sentence = []
		nums_in_text[sentence_counter] = 0
		end = sentence[-1]
		words = sentence[:-1].split(' ')
		new_sent = []
		org_sent = []
		non_vocab[sentence_counter] = 0
		vocab_sent = []
		for word in words:
			if(len(word)>0 and word != ' '):
				if(word[0] in nums):
					nums_in_text[sentence_counter] += 1
				else:
					if(word not in stop_words ): #remove the stop words
						new_sent.append(word.lower())	#remove the stop words
						if(word.lower() in vocab):
							vocab_sent.append(word.lower())
						else:
							non_vocab[sentence_counter] += 0.2
		if(len(new_sent)>0):
			articleNew[sentence_counter] = ' '.join(new_sent)
		vocab_s[sentence_counter] = ' '.join(vocab_sent)
		sentence_counter+=1

		


	word_to_ix = {}
	ix_to_word = {}
	vocab_size = len(vocab)

	for i, word in enumerate(vocab):
		word_to_ix[word] = i
		ix_to_word[i] = word

	model = CBOW(vocab_size, EMDEDDING_DIM)
	model.load_state_dict(torch.load(str(root)+"/Helpers/Resource/model.torch"))
	model.eval()

	bag_of_words_articleNew_sentences = []
	words_in_document = 0 # number of words in articleNew
	bag_of_words_articleNew = {}

	already_checked_words = []
	for sentence in articleNew:
		bag_of_words = {}
		words = sentence.split(' ')
		for word in words:
			word = word.lower()
			

			if(word in occurence_of_every_word_once_in_document 
				not in already_checked_words):
				occurence_of_every_word_once_in_document[word] += 1
				already_checked_words.append(word)
			else:
				occurence_of_every_word_once_in_document[word] = 1
				already_checked_words.append(word)


			words_in_document += 1
			if(word in bag_of_words):
				bag_of_words[word]+=1
			else:
				bag_of_words[word]=1


			if(word in bag_of_words_articleNew):
				bag_of_words_articleNew[word]+=1
			else:
				bag_of_words_articleNew[word]=1

		bag_of_words_articleNew_sentences.append(bag_of_words)
	bag_of_words_all.append(bag_of_words_articleNew_sentences)
		

	# Biggest TF * IDF
	BiggestTFIDF = {}
	TFIDF_articleNew =[]

	counter_sentence = 0
	TFIDF_D = {}
	for sentence in articleNew:
		already_checked_words = []
		if(counter_sentence == 0):
			pass
		else:
			for word, value in bag_of_words_all[-1][counter_sentence].items():
				if(word not in already_checked_words):
					TF = (bag_of_words_articleNew[word]/
						words_in_document)
					IDF = math.log(len(bag_of_words_all)/
						occurence_of_every_word_once_in_document[word])
					TFIDF = TF * IDF
					TFIDF_D[word] = TFIDF
					already_checked_words.append(word)
					try:
						if(TFIDF > BiggestTFIDF[word]):
							BiggestTFIDF[word] = TFIDF
					except:
						BiggestTFIDF[word] = TFIDF

		counter_sentence += 1

	num_of_sentences = math.floor((len(articleNew)/100)*15)
	
	features_list = []

	max_thematic_words_sentence = 0 
	longest_sentence = ''
	biggestSim = 0

	bar_counter = 0
	len_bar = int(len(articleNew)/10)
	end_bar = 10
	bar = '['
	print(bar+' '*end_bar+']', end="\r", flush=True)

	counter_sentence = 0
	for sentence in articleNew:
		feature_list = [] # all of the features
		if(counter_sentence == 0): # first sentence is the title
			pass
		else:
			words = sentence.split(' ')
			
			#0. title cosine simmilarty 
			feature_list.append(get_cosine(bag_of_words_all[-1][0],
			bag_of_words_all[-1][counter_sentence]))

			#1. Sentence position
			if (counter_sentence > num_of_sentences):
				feature_list.append(0)
			else:
				feature_list.append((num_of_sentences+1-counter_sentence)/
					num_of_sentences)

			#2. Term weight
			sum1=0
			sum2=0
			for word in words:
				word = word.lower()
				if(len(word)>0 and TFIDF_D[word]>0):
					sum1 += TFIDF_D[word]
					sum2 += BiggestTFIDF[word]
			
			
			if(sum2 > 0):
				feature_list.append(sum1/sum2)
			else:
				feature_list.append(0)

			#3. Thematic words part 1
			sent = vocab_s[counter_sentence]
			data = []
			Thematic = 0
			if(len(sent)>4):
				words = sent.split()
				for i in range(2, len(words) - 2):
					context = [words[i - 2], words[i - 1],
							   words[i + 1], words[i + 2]]
					target = words[i]
					data.append((context, target))
				for c,t in data:

					context_vector = make_context_vector(c, word_to_ix)
					a = model(context_vector).data.numpy()
					res = get_max_prob_result(a[0], ix_to_word)
					Thematic += get_cosine(create_BOW(res),create_BOW(t))
				Thematic += non_vocab[counter_sentence]
				feature_list.append(Thematic/len(sent))
			else:
				feature_list.append(len(sent)/15)

			#sum_of_thematic_words = 0
			#already_checked_words = []	
			#for word in words:	
			#	word = word.lower()
			#	if(len(word)>0 and TFIDF_D[word]>0):
			#		sum_of_thematic_words += TFIDF_D[word]
			#if(sum_of_thematic_words > max_thematic_words_sentence):
			#	max_thematic_words_sentence = sum_of_thematic_words
			#feature_list.append(sum_of_thematic_words)
			
			#4. Proper nouns
			nouns = 0
			for word in words:
				word = word.lower()
				try:
					if(word[0] not in nums and word not in sskj):
						nouns += 1
				except Exception as e:
					nouns += 1

			feature_list.append(nouns/len(sentence))

			#5. Sentence lenght part 1
			if(len(sentence) > len(longest_sentence)):
				longest_sentence = sentence 
			feature_list.append(len(sentence))

			#6. Sentence to sentence simillarty part 1
			simmilarty = 0
			for sentence_tmp in range(len(articleNew)):
				if(sentence_tmp != counter_sentence):
					simmilarty += get_cosine(bag_of_words_all[-1][sentence_tmp],
						bag_of_words_all[-1][counter_sentence])
			if(simmilarty>biggestSim):
				biggestSim = simmilarty
			feature_list.append(simmilarty)

			#7. Numerical data
			numbers = nums_in_text[counter_sentence]
			feature_list.append(numbers/len(sentence))


		counter_sentence+=1
		if(len(feature_list)>0):
			features_list.append(feature_list)
		bar_counter += 1
		if(bar_counter % len_bar == 0):
			bar += '='
			end_bar -= 1
			print(bar+' '*end_bar+']', end="\r", flush=True)

	
		
	# parts 2
	counter_sentence = 0
	#longest_sentence = features_list[0][5]
	for sentence in articleNew:
		if(counter_sentence == 0):
			pass
		else:
			#3. thematic words part 2
			#x = features_list[counter_sentence-1][3]
			
			#features_list[counter_sentence-1][3]=(x/
			#	max_thematic_words_sentence)

			#5. sentence lenght part 2
			features_list[counter_sentence-1][5] = (
				len(sentence)/len(longest_sentence))

			#6. sentence simmilarty part 2
			sim = features_list[counter_sentence-1][6]
			features_list[counter_sentence-1][6] = sim/biggestSim

		counter_sentence += 1

	print(bar+' '*end_bar+'] - COMPLETED', end="\n", flush=True)
		
	return features_list

def generalStatisticMethod_summary(article):
	sentece_score_list = []
	sentence_index = 0

	for sentence_vector in article:
		score = 0

		for feature_scalar in sentence_vector:
			score += feature_scalar
		sentece_score_list.append((score,sentence_index))
		sentence_index +=1

	
	sentece_score_list.sort(reverse=True)
	new_sentece_score_list = article
	how_many_sentences = math.ceil((len(article)*1.0)/100 * 40) # 40 percent
	sentence_index = 0
	for sentece_score in sentece_score_list:
		score,position = sentece_score
		included = 0
		if(sentence_index<how_many_sentences):
			included = 1
		sentence_index+=1
		new_sentece_score_list[position].append(included)

	return new_sentece_score_list

# Split the dataset by class values, returns a dictionary
def separate_by_class(dataset):
	separated = dict()
	for i in range(len(dataset)):
		vector = dataset[i]
		class_value = vector[-1]
		if (class_value not in separated):
			separated[class_value] = list()
		separated[class_value].append(vector)
	return separated
 
# Calculate the mean of a list of numbers
def mean(numbers):
	return sum(numbers)/float(len(numbers))
 
# Calculate the standard deviation of a list of numbers
def stdev(numbers):
	avg = mean(numbers)
	if(float(len(numbers)-1) != 0):
		variance = sum([(x-avg)**2 for x in numbers]) / float(len(numbers)-1)
		return sqrt(variance)
	else:
		variance = sum([(x-avg)**2 for x in numbers]) / 1
		return sqrt(variance)
 
# Calculate the mean, stdev and count for each column in a dataset
def summarize_dataset(dataset):
	summaries = [(mean(column), stdev(column), len(column)) for column in zip(*dataset)]
	del(summaries[-1])
	return summaries
 
# Split dataset by class then calculate statistics for each row
def summarize_by_class(dataset):
	separated = separate_by_class(dataset)
	summaries = dict()
	for class_value, rows in separated.items():
		summaries[class_value] = summarize_dataset(rows)
	return summaries
 
# Calculate the Gaussian probability distribution function for x
def calculate_probability(x, mean, stdev):
	if(stdev != 0):
		exponent = exp(-((x-mean)**2 / (2 * stdev**2 )))
		return (1 / (sqrt(2 * pi) * stdev)) * exponent
	else:
		exponent = exp(-((x-mean)**2 / (2 * 1**2 )))
		return (1 / (sqrt(2 * pi) * 1)) * exponent
	
 
# Calculate the probabilities of predicting each class for a given row
def calculate_class_probabilities(summaries, row):
	total_rows = sum([summaries[label][0][2] for label in summaries])
	probabilities = dict()
	for class_value, class_summaries in summaries.items():
		probabilities[class_value] = summaries[class_value][0][2]/float(total_rows)
		for i in range(len(class_summaries)):
			mean, stdev, _ = class_summaries[i]
			probabilities[class_value] *= calculate_probability(row[i], mean, stdev)

	return probabilities

def getNaiveBayes(article):

	bayes_score = copy.deepcopy(article)
	summaries_l = summarize_by_class(bayes_score)
	
	tuple_list = list()
	sentence_index = 0
	seventy = ceil((len(bayes_score)*1.0)/100 * 70) # 70 percent
	for sentence_vector in bayes_score:

		probabilities_s = calculate_class_probabilities(summaries_l, sentence_vector)
		
		diff = probabilities_s[1] - probabilities_s[0]

		tupl = (diff,sentence_index)

		tuple_list.append(tupl)
		sentence_index += 1

	tuple_list.sort(reverse=True)
	sentence_index = 0
	for tup in tuple_list:
		diff,sentence_position = tup
		if(sentence_index<seventy):
			bayes_score[sentence_position].append(1)
		else:
			bayes_score[sentence_position].append(0)
		sentence_index += 1

	return(bayes_score)

def fuzziefier(scalar):
	if(1.0>=scalar>0.9):
		return 10
	elif(0.9>scalar>0.8):
		return 9
	elif(0.8>scalar>0.7):
		return 8
	elif(0.7>scalar>0.6):
		return 7
	elif(0.6>scalar>0.5):
		return 6
	elif(0.5>scalar>0.4):
		return 5
	elif(0.4>scalar>0.3):
		return 4
	elif(0.3>scalar>0.2):
		return 3
	elif(0.2>scalar>0.1):
		return 2
	else:
		return 1

def fuzzification(article):

	fuzzified_features = []
	for sentence_vector in article:
		fuzzified_sentence_vector = sentence_vector
		for scalar in range(len(sentence_vector)):
			fuzzified_sentence_vector[scalar] = fuzziefier(sentence_vector[scalar])
		fuzzified_features.append(fuzzified_sentence_vector)
	return fuzzified_features

def fuzzy_logic(article):
	tuple_list = []
	fuzzy_score = []
	fuzzy_bias = copy.deepcopy(article)

	for sentence in article:
		fuzzy_score.append(0)

	sentence_index = 0
	for sentence_vector in article:

		endScore = 0

		importantFeatures = (sentence_vector[2]+sentence_vector[3]+
			sentence_vector[4]+sentence_vector[9])

		lessImportantFeatures = (sentence_vector[0]+sentence_vector[1]+sentence_vector[5]+
			sentence_vector[6]+sentence_vector[7]+sentence_vector[8])
		
		# Important features get promoted
		if(importantFeatures > 18):
			endScore = importantFeatures+100+lessImportantFeatures
		elif(importantFeatures > 9):
			endScore = importantFeatures+30+lessImportantFeatures
		elif(importantFeatures > 3):
			endScore = importantFeatures+10+lessImportantFeatures
		else:
			endScore = importantFeatures+lessImportantFeatures

		tuple_list.append((endScore,sentence_index))
		sentence_index += 1

	tuple_list.sort(reverse=True)

	how_many_sentences = ceil(len(article)/4) # 4 groups
	how_many_sentences_in_summary = ceil((len(article)*1.0)/100 * 40) # 40 percent	

	sentence_index = 0
	for tup in tuple_list:
		score,sentence_position = tup

		if(sentence_index<int(how_many_sentences*random.uniform(1.1,1.7))):
			fuzzy_score[sentence_position] = 3
		elif(sentence_index<int(how_many_sentences*random.uniform(2.,2.3))):
			fuzzy_score[sentence_position] = 2
		elif(sentence_index<int(how_many_sentences*random.uniform(3.2,4.1))):
			fuzzy_score[sentence_position] = 1
		elif(sentence_index<int(how_many_sentences*random.uniform(4.,4.1))):
			fuzzy_score[sentence_position] = 0
		else:
			fuzzy_score[sentence_position] = 0

		if(sentence_index<how_many_sentences_in_summary):
			fuzzy_bias[sentence_position].append(1)
		else:
			fuzzy_bias[sentence_position].append(0)
		
		sentence_index += 1


	return(fuzzy_bias,fuzzy_score)





