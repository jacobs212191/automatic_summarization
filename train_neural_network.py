import pickle
import tensorflow as tf
import math
import numpy as np
import os
import logging
import datetime

EPOCHS = 25
OPTIMIZER = 'adam'
#OPTIMIZER = 'rmsprop'

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)

File_object = open(r"NN/naive_bayes_sgm_pickled","rb")
features = pickle.load(File_object)
File_object.close()

File_object = open(r"NN/fuzzy_scores_pickled","rb")
score = pickle.load(File_object)
File_object.close()

# 80 %
train_x = [] 
train_y = [] 
# 20 %
test_x = [] 
test_y = [] 

how_many = int((len(features)/100) * 80)
num_train = 0
num_test = 0

for article in range(len(features)):
	for sentence in range(len(features[article])):
		if(article>how_many):
			num_test += 1
			test_x.append(np.array(features[article][sentence],dtype = 'float'))
			test_y.append(np.array(score[article][sentence],dtype = 'int32'))
		else:
			num_train += 1
			train_x.append(np.array(features[article][sentence],dtype = 'float'))
			train_y.append(np.array(score[article][sentence],dtype = 'int32'))
	dic = {0:0, 1:0, 2:0,3:0}
	for p in score[article]:
		dic[p] += 1
	print(dic)

print('\n\n\n')
print('Number of articles:'+str(len(features)))
print('Train data:'+str(how_many)+', number of sentences:'+str(num_train))
print('Test data:'+str(len(features)-how_many)+', number of sentences:'+str(num_test))



train_x = np.array(train_x,dtype = 'float') 
train_y = np.array(train_y,dtype = 'float')  
test_x = np.array(test_x,dtype = 'int32') 
test_y = np.array(test_y,dtype = 'int32') 

model = tf.keras.models.Sequential([
	tf.keras.layers.Dense(30, input_shape=(11,), activation='sigmoid'),
	tf.keras.layers.Dropout(0.2),
	tf.keras.layers.Dense(26, activation='sigmoid'),

	tf.keras.layers.Dense(22, activation='sigmoid'),
	tf.keras.layers.Dropout(0.2),
	tf.keras.layers.Dense(18, activation='sigmoid'),


	tf.keras.layers.Dense(14, activation='sigmoid'),
	tf.keras.layers.Dropout(0.2),

	tf.keras.layers.Dense(4, activation='softmax')
])

model.compile(optimizer=OPTIMIZER,
	loss='sparse_categorical_crossentropy',
	metrics=['accuracy'])

log_dir = 'NN/logs/fit' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S EP:"+str(EPOCHS)+' OP:'+OPTIMIZER)
tensorboard_calback = tf.keras.callbacks.TensorBoard(log_dir,histogram_freq=1)

model.fit(train_x, train_y, 
	epochs=EPOCHS,
	validation_data=(test_x,  test_y),
	callbacks = [tensorboard_calback])

model.save(r'NN/model.h5')
