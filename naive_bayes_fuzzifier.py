from math import sqrt
from math import pi
from math import exp
from math import ceil
from math import floor
import pickle
 
 
File_object = open(r"Summaries_result_vectors/general_statistical_method_pickled","rb")
features_by_article = pickle.load(File_object)
File_object.close()

File_object = open(r"NN/general_statistical_method_pickled","rb")
features_by_article_with_sgm = pickle.load(File_object)
File_object.close()

def fuzziefier(scalar):
	if(1.0>=scalar>0.9):
		return 10
	elif(0.9>scalar>0.8):
		return 9
	elif(0.8>scalar>0.7):
		return 8
	elif(0.7>scalar>0.6):
		return 7
	elif(0.6>scalar>0.5):
		return 6
	elif(0.5>scalar>0.4):
		return 5
	elif(0.4>scalar>0.3):
		return 4
	elif(0.3>scalar>0.2):
		return 3
	elif(0.2>scalar>0.1):
		return 2
	else:
		return 1

# Split the dataset by class values, returns a dictionary
def separate_by_class(dataset):
	separated = dict()
	for i in range(len(dataset)):
		vector = dataset[i]
		class_value = vector[-1]
		if (class_value not in separated):
			separated[class_value] = list()
		separated[class_value].append(vector)
	return separated
 
# Calculate the mean of a list of numbers
def mean(numbers):
	return sum(numbers)/float(len(numbers))
 
# Calculate the standard deviation of a list of numbers
def stdev(numbers):
	avg = mean(numbers)
	if(float(len(numbers)-1) != 0):
		variance = sum([(x-avg)**2 for x in numbers]) / float(len(numbers)-1)
		return sqrt(variance)
	else:
		return 0


 
# Calculate the mean, stdev and count for each column in a dataset
def summarize_dataset(dataset):
	summaries = [(mean(column), stdev(column), len(column)) for column in zip(*dataset)]
	del(summaries[-1])
	return summaries
 
# Split dataset by class then calculate statistics for each row
def summarize_by_class(dataset):
	separated = separate_by_class(dataset)
	summaries = dict()
	for class_value, rows in separated.items():
		summaries[class_value] = summarize_dataset(rows)
	return summaries
 
# Calculate the Gaussian probability distribution function for x
def calculate_probability(x, mean, stdev):
	if(stdev != 0):
		exponent = exp(-((x-mean)**2 / (2 * stdev**2 )))
		return (1 / (sqrt(2 * pi) * stdev)) * exponent
	return 0
 
# Calculate the probabilities of predicting each class for a given row
def calculate_class_probabilities(summaries, row):
	total_rows = sum([summaries[label][0][2] for label in summaries])
	probabilities = dict()
	for class_value, class_summaries in summaries.items():
		probabilities[class_value] = summaries[class_value][0][2]/float(total_rows)
		for i in range(len(class_summaries)):
			mean, stdev, _ = class_summaries[i]
			probabilities[class_value] *= calculate_probability(row[i], mean, stdev)
	return probabilities

bar_counter = 0
len_bar = int(len(features_by_article)/10)
end_bar = 10
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

article_index = 0
for article in features_by_article:
	
	sentence_index = 0
	summaries = summarize_by_class(article)
	tuple_list = list()

	for sentence_vector in article:

		probabilities = calculate_class_probabilities(summaries, sentence_vector)
		
		diff = probabilities[1] - probabilities[0]
		
		tupl = (diff,sentence_index)
		tuple_list.append(tupl)

		# Fuzzification
		for scalar in range(len(sentence_vector)):
			num = features_by_article_with_sgm[article_index][sentence_index][scalar]
			features_by_article_with_sgm[article_index][sentence_index][scalar]=fuzziefier(num)

		sentence_index += 1
		
	sentence_index = 0
	tuple_list.sort(reverse=True)

	isFloat = False
	if(type(len(article)/10) == float):
		isFloat = True
	how_many_sentences = floor(len(article)/10) # 10 groups


	for tup in tuple_list:
		diff,sentence_position = tup
		if(sentence_index<how_many_sentences):
			features_by_article_with_sgm[article_index][sentence_position].append(10)
		elif(sentence_index<how_many_sentences*2):
			features_by_article_with_sgm[article_index][sentence_position].append(9)
		elif(sentence_index<how_many_sentences*3):
			features_by_article_with_sgm[article_index][sentence_position].append(8)
		elif(sentence_index<how_many_sentences*4):
			features_by_article_with_sgm[article_index][sentence_position].append(7)
		elif(sentence_index<how_many_sentences*5):
			features_by_article_with_sgm[article_index][sentence_position].append(6)
		elif(sentence_index<how_many_sentences*6):
			features_by_article_with_sgm[article_index][sentence_position].append(5)
		elif(isFloat):
			isFloat=False
			features_by_article_with_sgm[article_index][sentence_position].append(5)
		elif(sentence_index<how_many_sentences*7):
			features_by_article_with_sgm[article_index][sentence_position].append(4)
		elif(sentence_index<how_many_sentences*8):
			features_by_article_with_sgm[article_index][sentence_position].append(3)
		elif(sentence_index<how_many_sentences*9):
			features_by_article_with_sgm[article_index][sentence_position].append(2)
		else:
			features_by_article_with_sgm[article_index][sentence_position].append(1)
		sentence_index += 1

	article_index +=1
	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)

File_object = open(r'NN/naive_bayes_Fuzziefied_pickled', 'wb')
pickle.dump(features_by_article_with_sgm, File_object,-1)
File_object.close()

print(bar+' '*end_bar+'] - COMPLETED', end="\n", flush=True)