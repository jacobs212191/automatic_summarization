import pickle
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM, BatchNormalization
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import ModelCheckpoint
import tensorflow as tf
import math
import numpy as np
import os
import logging
import datetime
from collections import deque

EPOCHS = 10
BATCH_SIZE = 10
OPTIMIZER = 'adam'
SEQ = 3
#OPTIMIZER = 'rmsprop'

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)

File_object = open(r"NN/naive_bayes_sgm_pickled","rb")
features = pickle.load(File_object)
File_object.close()

File_object = open(r"NN/fuzzy_scores_pickled","rb")
score = pickle.load(File_object)
File_object.close()

# 80 %
train_x = [] 
train_y = [] 
# 20 %
test_x = [] 
test_y = [] 


how_many = (len(features)/100) * 80
seq_of_sent = deque(maxlen=SEQ)

for article in range(len(features)):
	for sentence in range(len(features[article])):
		if(sentence<SEQ-1):
			seq_of_sent.append(np.array([0,0,0,0,0,0,0,0,0,0],dtype = 'int32'))
		if(article>how_many):
			seq_of_sent.append(np.array(features[article][sentence],dtype = 'int32'))
			if len(seq_of_sent) == SEQ:
				test_x.append(seq_of_sent)
				test_y.append(np.array(score[article][sentence],dtype = 'int32'))
		else:
			seq_of_sent.append(np.array(features[article][sentence],dtype = 'int32'))
			if len(seq_of_sent) == SEQ:
				train_x.append(np.array(seq_of_sent))
				train_y.append(np.array(score[article][sentence],dtype = 'int32'))

train_x = np.array(train_x,dtype = 'int32') 
train_y = np.array(train_y,dtype = 'int32')  
test_x = np.array(test_x,dtype = 'int32') 
test_y = np.array(test_y,dtype = 'int32') 

print(train_x.shape[1:])

model = Sequential()

model.add(LSTM(50, input_shape=(train_x.shape[1:]), return_sequences=True))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(LSTM(50, return_sequences=True))
model.add(Dropout(0.1))
model.add(BatchNormalization())

model.add(LSTM(50))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(Dense(50, activation='relu'))
model.add(Dropout(0.2))

model.add(Dense(50, activation='relu'))
model.add(Dropout(0.2))

model.add(Dense(4, activation='softmax'))

#opt = tf.keras.optimizers.RMSprop(
#    learning_rate=0.001, rho=0.9, momentum=0.0, epsilon=1e-07
#)

opt = tf.keras.optimizers.Adam(lr=0.001, decay=1e-6)

# Compile model
model.compile(
    loss='sparse_categorical_crossentropy',
    optimizer=OPTIMIZER,
    metrics=['accuracy']
)

log_dir = 'NN/logsRNN/fit' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S EP:"+str(EPOCHS)+' OP:'+OPTIMIZER)
#tensorboard_calback = tf.keras.callbacks.TensorBoard(log_dir,histogram_freq=1)
filepath = "RNN_Final-{epoch:02d}-{val_accuracy:.3f}"  # unique file name that will include 
#the epoch and the validation acc for that epoch
checkpoint = ModelCheckpoint("NN/models/{}.model".format(filepath, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')) # saves only the best ones
tensorboard = TensorBoard(log_dir)

# Train model
history = model.fit(
    train_x, train_y,
    batch_size=BATCH_SIZE,
    epochs=EPOCHS,
    validation_data=(test_x,  test_y),
    callbacks=[tensorboard, checkpoint],
)
'''
model.fit(train_x, train_y, 
	epochs=EPOCHS,
	validation_data=(test_x,  test_y),
	callbacks = [tensorboard_calback])
'''
model.save(r'NN/modelRNN.h5')

#os.system('python3 Testing/test_neural_network.py')