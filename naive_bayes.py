from math import sqrt
from math import pi
from math import exp
from math import ceil
import pickle
import numpy as np
import copy
import random

File_object = open(r"Summaries_result_vectors/general_statistical_method_pickled","rb")
features_by_article = pickle.load(File_object)
File_object.close()

File_object = open(r"NN/general_statistical_method_pickled","rb")
features_by_article_with_sgm = pickle.load(File_object)
File_object.close()

thirty_percent = copy.deepcopy(features_by_article)
forty_percent = copy.deepcopy(features_by_article)
fifty_percent = copy.deepcopy(features_by_article)

# Split the dataset by class values, returns a dictionary
def separate_by_class(dataset):
	separated = dict()
	for i in range(len(dataset)):
		vector = dataset[i]
		class_value = vector[-1]
		if (class_value not in separated):
			separated[class_value] = list()
		separated[class_value].append(vector)
	return separated
 
# Calculate the mean of a list of numbers
def mean(numbers):
	return sum(numbers)/float(len(numbers))
 
# Calculate the standard deviation of a list of numbers
def stdev(numbers):
	avg = mean(numbers)
	if(float(len(numbers)-1) != 0):
		variance = sum([(x-avg)**2 for x in numbers]) / float(len(numbers)-1)
		return sqrt(variance)
	else:
		#variance = sum([(x-avg)**2 for x in numbers]) / 1
		#return sqrt(variance)
		return 0
 
# Calculate the mean, stdev and count for each column in a dataset
def summarize_dataset(dataset):
	summaries = [(mean(column), stdev(column), len(column)) for column in zip(*dataset)]
	del(summaries[-1])
	return summaries
 
# Split dataset by class then calculate statistics for each row
def summarize_by_class(dataset):
	separated = separate_by_class(dataset)
	summaries = dict()
	for class_value, rows in separated.items():
		summaries[class_value] = summarize_dataset(rows)
	return summaries
 
# Calculate the Gaussian probability distribution function for x
def calculate_probability(x, mean, stdev):
	if(stdev != 0):
		exponent = exp(-((x-mean)**2 / (2 * stdev**2 )))
		return (1 / (sqrt(2 * pi) * stdev)) * exponent
	else:
		#exponent = exp(-((x-mean)**2 / (2 * 1**2 )))
		#return (1 / (sqrt(2 * pi) * 1)) * exponent
		return 0
	
 
# Calculate the probabilities of predicting each class for a given row
def calculate_class_probabilities(summaries, row):
	total_rows = sum([summaries[label][0][2] for label in summaries])
	probabilities = dict()
	for class_value, class_summaries in summaries.items():
		probabilities[class_value] = summaries[class_value][0][2]/float(total_rows)
		for i in range(len(class_summaries)):
			mean, stdev, _ = class_summaries[i]
			probabilities[class_value] *= calculate_probability(row[i], mean, stdev)
	return probabilities

bar_counter = 0
len_bar = int(len(features_by_article)/10)
end_bar = 10
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

article_index = 0
for article in features_by_article:

	sentence_index = 0
	summaries = summarize_by_class(article)

	ten = ceil((len(article)*1.0)/100 * 30) # 10 percent
	how_many_sentences = ceil((len(article)*1.0)/100 * 20) # 20 percent
	thirty = ceil((len(article)*1.0)/100 * 30) # 30 percent
	forty = ceil((len(article)*1.0)/100 * 40) # 40 percent
	fifty = ceil((len(article)*1.0)/100 * 50) # 50 percent
	sixty = ceil((len(article)*1.0)/100 * 60) # 60 percent
	seventy = ceil((len(article)*1.0)/100 * 70) # 70 percent
	eighty = ceil((len(article)*1.0)/100 * 80) # 80 percent
	ninghty = ceil((len(article)*1.0)/100 * 90) # 90 percent


	tuple_list = list()
	scores = []
	for sentence_vector in article:

		probabilities = calculate_class_probabilities(summaries, sentence_vector)
			
		diff = probabilities[1] - probabilities[0]
		scores.append(diff) 
		tupl = (diff,sentence_index)
		tuple_list.append(tupl)

		sentence_index += 1
		
	#scores = softmax(scores)
	sentence_index = 0
	tuple_list.sort(reverse=True)
	for tup in tuple_list:
		diff,sentence_position = tup
		
		
		
		if(sentence_index<how_many_sentences):
			features_by_article[article_index][sentence_position][-1] = 1
			#features_by_article_with_sgm[article_index][sentence_position].append(1)
		else:
			features_by_article[article_index][sentence_position][-1] = 0
			#features_by_article_with_sgm[article_index][sentence_position].append(0)

		if(sentence_index<thirty):
			thirty_percent[article_index][sentence_position] = 1
		else:
			thirty_percent[article_index][sentence_position] = 0

		

			
		if(forty<=sentence_index<=sixty):
			fort = random.uniform(0.5,0.6)

		if(how_many_sentences<=sentence_index<=thirty):
			fort = random.uniform(0.85,0.93)
		if(ten<=sentence_index<=how_many_sentences):
			fort = random.uniform(0.9,1.)
		if(sentence_index<ten):
			fort = 1.

		if(sentence_index<forty):
			forty_percent[article_index][sentence_position] = 1
			fort = random.uniform(0.68,0.75)
		else:
			forty_percent[article_index][sentence_position] = 0

		fif = 0.
		if(sentence_index<fifty):
			fifty_percent[article_index][sentence_position] = 1
			fif = 1.
		else:
			fifty_percent[article_index][sentence_position] = 0
			fif = 0.
		
		if(sentence_index<sixty):
			fif = 1.
		else:
			fif = 0.

		features_by_article_with_sgm[article_index][sentence_position].append(fif)

		sentence_index += 1

	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)	
	article_index +=1



File_object = open(r'Summaries_result_vectors/naive_bayes_pickled', 'wb')
pickle.dump(features_by_article, File_object,-1)
File_object.close()

File_object = open(r'NN/naive_bayes_sgm_pickled', 'wb')
pickle.dump(features_by_article_with_sgm, File_object,-1)
File_object.close()

#File_object = open(r'NN/NB_supp_piclke', 'wb')
#pickle.dump(features_by_article_with_sgm, File_object,-1)
#File_object.close()

File_object = open(r'Testing/eval/nb/thirty_pickle', 'wb')
pickle.dump(thirty_percent, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/nb/forty_pickle', 'wb')
pickle.dump(forty_percent, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/nb/fifty_pickle', 'wb')
pickle.dump(fifty_percent, File_object,-1)
File_object.close()

print(bar+' '*end_bar+'] - COMPLETED', end="\n", flush=True)