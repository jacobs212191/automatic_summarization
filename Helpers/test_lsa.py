import os.path
from gensim import corpora
from gensim.models import LsiModel
from gensim.models.coherencemodel import CoherenceModel
import matplotlib.pyplot as plt
import pickle

File_object = open(r"Resource/vocab_pickle","rb")
vocab = pickle.load(File_object)
File_object.close()

File_object = open(r"Resource/corpus_pickle","rb")
corpus = pickle.load(File_object)
File_object.close()

File_object = open(r"Resource/preProcessed_pickle","rb")
preProcessed_pickle = pickle.load(File_object)
File_object.close()

def prepare_corpus(doc_clean):
	"""
	Input  : clean document
	Purpose: create term dictionary of our courpus and Converting
	list of documents (corpus) into Document Term Matrix

	Output : term dictionary and Document Term Matrix
	"""
	# Creating the term dictionary of our courpus, where every unique term is assigned 
	#an index. dictionary = corpora.Dictionary(doc_clean)
	dictionary = corpora.Dictionary(doc_clean)
	
	# Converting list of documents (corpus) into Document Term Matrix using dictionary 
	#prepared above.
	doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean]
	# generate LDA model
	return dictionary,doc_term_matrix

def create_gensim_lsa_model(doc_clean,number_of_topics,words):
	"""
	Input  : clean document, number of topics and number of words associated with each topic
	Purpose: create LSA model using gensim
	Output : return LSA model
	"""
	dictionary,doc_term_matrix=prepare_corpus(doc_clean)
	# generate LSA model
	lsamodel = LsiModel(doc_term_matrix, num_topics=number_of_topics, id2word = dictionary)  # train model
	print(lsamodel.print_topics(num_topics=number_of_topics, num_words=words))
	return lsamodel

def compute_coherence_values(dictionary, doc_term_matrix, doc_clean, stop, start=2, step=3):
	"""
	Input   : dictionary : Gensim dictionary
			  corpus : Gensim corpus
			  texts : List of input texts
			  stop : Max num of topics
	purpose : Compute c_v coherence for various number of topics
	Output  : model_list : List of LSA topic models
			  coherence_values : Coherence values corresponding to the LDA model with respective number of topics
	"""

	bar_counter = 0
	end_bar = 15
	len_bar = int((stop-start)/end_bar)*step
	bar = '['
	print(bar+' '*end_bar+']', end="\r", flush=True)

	coherence_values = []
	model_list = []
	for num_topics in range(start, stop, step):

		# generate LSA model
		model = LsiModel(doc_term_matrix, num_topics=num_topics, id2word = dictionary)  
		# train model
		model_list.append(model)
		coherencemodel = CoherenceModel(model=model, texts=doc_clean, dictionary=dictionary, 
			coherence='c_v')
		coherence_values.append(coherencemodel.get_coherence())

		bar_counter += 1
		if(bar_counter % len_bar == 0):
			bar += '='
			end_bar -= 1
			print(bar+' '*end_bar+']', end="\r", flush=True)
			
	return model_list, coherence_values

def plot_graph(doc_clean,start, stop, step):
	dictionary,doc_term_matrix=prepare_corpus(doc_clean)
	model_list, coherence_values = compute_coherence_values(dictionary, doc_term_matrix,doc_clean,
		stop, start, step)

	# Show graph
	x = range(start, stop, step)
	plt.plot(x, coherence_values)
	plt.xlabel("Number of Topics")
	plt.ylabel("Coherence score")
	plt.legend(("coherence_values"), loc='best')
	plt.show()

start,stop,step=5,500,5
plot_graph(corpus,start,stop,step)