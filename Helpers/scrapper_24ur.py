# coding: utf-8
from __future__ import unicode_literals

import pickle
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Firefox()

PAGES = 150 # each page takes 2 minutes, so RUNTIME = #PAGES * 2 minutes

bar_counter = 0
len_bar = int(PAGES/10)
end_bar = 15
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

articles = []

for page in range(1,PAGES):

	tryB = True
	while(tryB):
		try:
			driver.get("https://www.24ur.com/arhiv/novice?stran="+str(page))

			time.sleep(2)
			elements = len(driver.find_elements_by_xpath("/html/body/onl-root/div[1]/div[3]/\
				div[2]/onl-archive/div/div/div/main/div/div[2]/a"))
			tryB = False
		except:
			continue

	
	
	for elm in range(elements):
		article = []
		article_done = True
		try:
			elems = driver.find_elements_by_xpath("/html/body/onl-root/div[1]/div[3]/div[2]/\
				onl-archive/div/div/div/main/div/div[2]/a")
			elems[elm].click()
			time.sleep(2)

			title = driver.find_element_by_xpath('/html/body/onl-root/div[1]/div[3]/div[2]/\
				onl-article/div[2]/div/div/main/div[1]/h1')
			article.append(title.text)

			summary = driver.find_element_by_xpath('/html/body/onl-root/div[1]/div[3]/div[2]/\
				onl-article/div[2]/div/div/main/div[3]/div')
			article.append(summary.text)
			
			spans = driver.find_elements_by_xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-article/div[2]/\
				div/div/main/div[3]/onl-article-body/span')
			

			for s in range(1,len(spans)):
				paragraphs = driver.find_elements_by_xpath('/html/body/onl-root/div[1]/div[3]/div[2]/\
					onl-article/div[2]/div/div/main/div[3]/onl-article-body/span['+str(s)+']/p')

				for p in paragraphs:
					article.append(p.text)
		except:
			article_done = False
			
		if(len(article)>0 and article_done):
			articles.append(article)

		driver.back()
		time.sleep(2)

	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)
		

driver.close()

File_object = open(r'Helpers/dataset_raw/articles_everything_pickled', 'wb')
pickle.dump(articles, File_object,-1)
File_object.close()

print(bar+' '*end_bar+'] - COMPLETED', end="\n", flush=True)