import torch
import torch.nn as nn
import numpy as np
import pickle

File_object = open(r"Resource/vocab_pickle","rb")
vocab = pickle.load(File_object)
File_object.close()

File_object = open(r"Resource/corpus_pickle","rb")
corpus = pickle.load(File_object)
File_object.close()

File_object = open(r"Resource/preProcessed_pickle","rb")
preProcessed_pickle = pickle.load(File_object)
File_object.close()

class CBOW(torch.nn.Module):

    def __init__(self, vocab_size, embedding_dim):
        super(CBOW, self).__init__()

        #out: 1 x emdedding_dim
        self.embeddings = nn.Embedding(vocab_size, embedding_dim)

        self.linear1 = nn.Linear(embedding_dim, 128)

        self.activation_function1 = nn.ReLU()
        
        #out: 1 x vocab_size
        self.linear2 = nn.Linear(128, vocab_size)

        self.activation_function2 = nn.LogSoftmax(dim = -1)
        

    def forward(self, inputs):
        embeds = sum(self.embeddings(inputs)).view(1,-1)
        out = self.linear1(embeds)
        out = self.activation_function1(out)
        out = self.linear2(out)
        out = self.activation_function2(out)
        return out

    def get_word_emdedding(self, word):
        word = torch.LongTensor([word_to_ix[word]])
        return self.embeddings(word).view(1,-1)

def make_context_vector(context, word_to_ix):
    idxs = [word_to_ix[w] for w in context]
    return torch.tensor(idxs, dtype=torch.long)

def get_index_of_max(input):
    index = 0
    for i in range(1, len(input)):
        if input[i] > input[index]:
            index = i 
    return index

def get_max_prob_result(input, ix_to_word):
    return ix_to_word[get_index_of_max(input)]

CONTEXT_SIZE = 2  # 2 words to the left, 2 to the right
EMDEDDING_DIM = 100

vocab_size = len(vocab)

model = CBOW(vocab_size, EMDEDDING_DIM)
model.load_state_dict(torch.load("Resource/model.torch"))

word_to_ix = {}
ix_to_word = {}

vocab_size = len(vocab)

for i, word in enumerate(vocab):
    word_to_ix[word] = i
    ix_to_word[i] = word

data = []
for words in corpus:
    for i in range(2, len(words) - 2):
        context = [words[i - 2], words[i - 1],
                   words[i + 1], words[i + 2]]
        target = words[i]
        data.append((context, target))

data = []
for article in preProcessed_pickle[:1]:
    for sentence in article:
        if(len(sentence)>4):
            words = sentence.split()
            for i in range(2, len(words) - 2):
                context = [words[i - 2], words[i - 1],
                           words[i + 1], words[i + 2]]
                target = words[i]
                data.append((context, target))
            for c,t in data:
                context_vector = make_context_vector(c, word_to_ix)
                a = model(context_vector).data.numpy()
                print('Context: {}, target: {}'.format(c,t))
                print('Prediction: {}\n'.format(get_max_prob_result(a[0], ix_to_word)))