# coding: utf-8
from __future__ import unicode_literals

import pickle
from io import open
import sys

File_object = open(sys.argv[1]+'_pickled', 'wb')
f = open(sys.argv[1], 'r', encoding="utf8") #, encoding="utf8"

ls = []

for line in f:
	ls.append(line)

pickle.dump(ls, File_object,-1)
f.close
File_object.close()