# coding: utf-8

import math
import pickle
import numpy as np
import copy

def softmax(x):
	softmax = np.exp(x)/sum(np.exp(x))
	softmax[np.isnan(softmax)] = 0
	return softmax

File_object = open(r"Helpers/features_pickled","rb")
features_by_article = pickle.load(File_object)
File_object.close()

features_by_article_scores = copy.deepcopy(features_by_article)
thirty_percent = copy.deepcopy(features_by_article)
forty_percent = copy.deepcopy(features_by_article)
fifty_percent = copy.deepcopy(features_by_article)

bar_counter = 0
len_bar = int(len(features_by_article)/10)
end_bar = 10
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

article_score_list = []
article_index = 0
for article in features_by_article:
	sentece_score_list = []
	sentence_index = 0

	scores = []
	for sentence_vector in article:
		score = 0
		for feature_scalar in sentence_vector:
			score += feature_scalar
		scores.append(score)
		sentece_score_list.append((score,sentence_index))
		
		sentence_index +=1

	sm = softmax(scores)
	sentece_score_list.sort(reverse=True)
	new_sentece_score_list = []

	how_many_sentences = math.ceil((len(article)*1.0)/100 * 20) # 20 percent
	thirty = math.ceil((len(article)*1.0)/100 * 30) # 30 percent
	forty = math.ceil((len(article)*1.0)/100 * 40) # 40 percent
	fifty = math.ceil((len(article)*1.0)/100 * 50) # 50 percent

	thirtyL = []
	fortyL = []
	fiftyL = []

	sentence_index = 0
	for sentece_score in sentece_score_list:
		score,position = sentece_score
		included = 0
		if(sentence_index<how_many_sentences):
			included = 1

		if(sentence_index<thirty):
			
			thirtyL.append((position,1))
		else:
			
			thirtyL.append((position,0))

		if(sentence_index<forty):
			features_by_article_scores[article_index][position].append(1)
			fortyL.append((position,1))
		else:
			features_by_article_scores[article_index][position].append(0)
			fortyL.append((position,0))

		if(sentence_index<fifty):
			
			fiftyL.append((position,1))
		else:
			
			fiftyL.append((position,0))

		sentence_index+=1
		new_sentece_score_list.append((position,included))

	new_sentece_score_list.sort()
	thirtyL.sort()
	fortyL.sort()
	fiftyL.sort()
	sentece_score_list = []

	for position, included in new_sentece_score_list:
		thirty_percent[article_index][position] = thirtyL[position][1]
		forty_percent[article_index][position] = fortyL[position][1]
		fifty_percent[article_index][position] = fiftyL[position][1]
		#features_by_article_scores[article_index][position].append(thirtyL[position][1])
		#features_by_article_scores[article_index][position].append(fiftyL[position][1])
		#features_by_article_scores[article_index][position].append(sm[position])
		#features_by_article_scores[article_index][position].append(included)
		features_by_article[article_index][position].append(included)
	
	article_index+=1

	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)	


File_object = open(r'Summaries_result_vectors/general_statistical_method_pickled', 'wb')
pickle.dump(features_by_article, File_object,-1)
File_object.close()

File_object = open(r'NN/general_statistical_method_pickled', 'wb')
pickle.dump(features_by_article_scores, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/gsm/thirty_pickle', 'wb')
pickle.dump(thirty_percent, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/gsm/forty_pickle', 'wb')
pickle.dump(forty_percent, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/gsm/fifty_pickle', 'wb')
pickle.dump(fifty_percent, File_object,-1)
File_object.close()

print(bar+' '*end_bar+'] - COMPLETED', end="\n", flush=True)
	
	