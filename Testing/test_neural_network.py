import pickle
import tensorflow as tf
import numpy as np
import datetime
from math import ceil
from pathlib import Path
import logging
import os
import copy

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)

root = Path(__file__).absolute().parents[1]

File_object = open(str(root)+"/NN/naive_bayes_sgm_pickled","rb")
features = pickle.load(File_object)
File_object.close()

File_object = open(str(root)+"/Helpers/preprocessed_text_pickled","rb")
preProcessed_text = pickle.load(File_object)
File_object.close()

thirty_percent = copy.deepcopy(features)
forty_percent = copy.deepcopy(features)
fifty_percent = copy.deepcopy(features)

model = tf.keras.models.load_model(str(root)+'/NN/model.h5')
time = datetime.datetime.now()
features_by_article_shorter = features[:]

summaries = open('test_summaries/neural_network', 'w', encoding="utf8")
predic = []

bar_counter = 0
len_bar = int(len(features_by_article_shorter)/10)
end_bar = 10
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

article_index = 0
for article in features_by_article_shorter:
	
	summaries.write('Title: '+preProcessed_text[article_index][0]+'\n\n')
	
	predictions = []
	predictions_model = model.predict(np.array(article,dtype = 'int32'))

	sentence_index = 1
	for prediction in predictions_model:
		pred_score = prediction[2] - prediction[1] - prediction[0]
		predictions.append((np.argmax(prediction),sentence_index,pred_score))
		sentence_index+=1


	if(article_index<30):
		dic = {0:0, 1:0, 2:0, 3:0,4:0}
		for p,s,i in predictions:
			dic[p] += 1
		print(str(dic[0])+' '+str(dic[1])+' '+str(dic[2])+' '+str(dic[3]))

		
	predictions.sort(reverse=True)

	how_many_sentences_in_summary = ceil((len(article)*1.0)/100 * 20) # 20 percent
	thirty = ceil((len(article)*1.0)/100 * 30) # 30 percent
	forty = ceil((len(article)*1.0)/100 * 40) # 40 percent
	fifty = ceil((len(article)*1.0)/100 * 50) # 50 percent

	included =[]
	sentence_index = 0
	predictionnum = []
	for tup in predictions:
		pred, sentence_position, i  = tup
		if(sentence_index<how_many_sentences_in_summary):
			predictionnum.append((sentence_position-1,1))
			included.append(sentence_position)
		else:
			predictionnum.append((sentence_position-1,0))

		if(sentence_index<thirty):
			thirty_percent[article_index][sentence_position-1] = 1
		else:
			thirty_percent[article_index][sentence_position-1] = 0

		if(sentence_index<forty):
			forty_percent[article_index][sentence_position-1] = 1
		else:
			forty_percent[article_index][sentence_position-1] = 0

		if(sentence_index<fifty):
			fifty_percent[article_index][sentence_position-1] = 1
		else:
			fifty_percent[article_index][sentence_position-1] = 0

		sentence_index+=1

	included.sort()
	for sentence_position in included:
		
		summaries.write(preProcessed_text[article_index][sentence_position]+' ')

	summaries.write('\n\nOriginal:\n')
	summaries.write(str(preProcessed_text[article_index]))
	summaries.write('\n-------------------------------------------\n')

	
	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)
	article_index+=1
	predic.append(predictionnum)
summaries.close()

File_object = open(r'predictions_pickled', 'wb')
pickle.dump(predic, File_object,-1)
File_object.close()

File_object = open(r'eval/nn/thirty_pickle', 'wb')
pickle.dump(thirty_percent, File_object,-1)
File_object.close()

File_object = open(r'eval/nn/forty_pickle', 'wb')
pickle.dump(forty_percent, File_object,-1)
File_object.close()

File_object = open(r'eval/nn/fifty_pickle', 'wb')
pickle.dump(fifty_percent, File_object,-1)