# coding: utf-8

# Membership functions:
# https://pythonhosted.org/scikit-fuzzy/_modules/skfuzzy/membership/generatemf.html


import pickle
from math import floor
from math import ceil
import random
import copy

#Feature postions:
#0. title cosine simmilarty 
#1. Sentence position
#2. Term weight
#3. Thematic words
#4. Proper nouns
#5. Sentence lenght
#6. Sentence to sentence cosine simillarty
#7. Numerical data
#8. General statistical method
#9. Bayesian clasifier

File_object = open(r"NN/naive_bayes_Fuzziefied_pickled","rb")
features_by_article = pickle.load(File_object)
File_object.close()

File_object = open(r"NN/naive_bayes_sgm_pickled","rb")
features = pickle.load(File_object)
File_object.close()

thirty_percent = copy.deepcopy(features_by_article)
forty_percent = copy.deepcopy(features_by_article)
fifty_percent = copy.deepcopy(features_by_article)

bar_counter = 0
len_bar = int(len(features_by_article)/10)
end_bar = 10
bar = '['
print(bar+' '*end_bar+']', end="\r", flush=True)

half_good = []
fuzzy_scores = []
article_index = 0
for article in features_by_article:
	
	tuple_list = []
	fuzzy_score = []

	for sentence in article:
		fuzzy_score.append(0)

	sentence_index = 0
	for sentence_vector in article:

		endScore = 0

		importantFeatures = (sentence_vector[2]+sentence_vector[3]+
			sentence_vector[4]+sentence_vector[9])

		lessImportantFeatures = (sentence_vector[0]+sentence_vector[1]+sentence_vector[5]+
			sentence_vector[6]+sentence_vector[7]+sentence_vector[8])
		
		# Important features get promoted
		if(importantFeatures > 18):
			endScore = importantFeatures+100+lessImportantFeatures
		elif(importantFeatures > 9):
			endScore = importantFeatures+30+lessImportantFeatures
		elif(importantFeatures > 3):
			endScore = importantFeatures+10+lessImportantFeatures
		else:
			endScore = importantFeatures+lessImportantFeatures

		tuple_list.append((endScore,sentence_index))
		sentence_index += 1

	tuple_list.sort(reverse=True)

	
	how_many_sentences = floor(len(article)/4) # classes 
	how_many_sentences_in_summary = ceil((len(article)*1.0)/100 * 20) # 20 percent
	thirty = ceil((len(article)*1.0)/100 * 30) # 30 percent
	forty = ceil((len(article)*1.0)/100 * 40) # 40 percent
	fifty = ceil((len(article)*1.0)/100 * 50) # 50 percent

	half_good_sent = ceil(len(article)/2.3)

	sentence_index = 0
	for tup in tuple_list:
		score,sentence_position = tup

		if(sentence_index<int(how_many_sentences*random.uniform(1.2,1.4))):
			fuzzy_score[sentence_position] = 3
		elif(sentence_index<int(how_many_sentences*random.uniform(2.,2.5))):
			fuzzy_score[sentence_position] = 2
		elif(sentence_index<int(how_many_sentences*random.uniform(3.2,3.5))):
			fuzzy_score[sentence_position] = 1
		elif(sentence_index<int(how_many_sentences*random.uniform(3.5,3.7))):
			fuzzy_score[sentence_position] = 0
		else:
			fuzzy_score[sentence_position] = 0
		
		if(sentence_index<thirty):
			
			thirty_percent[article_index][sentence_position] = 1
		else:
			
			thirty_percent[article_index][sentence_position] = 0

		if(sentence_index<forty):
			features[article_index][sentence_position].append(1.)
			forty_percent[article_index][sentence_position] = 1
		else:
			features[article_index][sentence_position].append(0.)
			forty_percent[article_index][sentence_position] = 0

		if(sentence_index<fifty):
			#features[article_index][sentence_position].append(1.)
			fifty_percent[article_index][sentence_position] = 1
		else:
			#features[article_index][sentence_position].append(0.)
			fifty_percent[article_index][sentence_position] = 0

		#fort = forty_percent[article_index][sentence_position]
			
		#if(sentence_index<half_good_sent):
			#features[article_index][sentence_position].append(1)
		#else:
			#features[article_index][sentence_position].append(0)


		#if(sentence_index<how_many_sentences_in_summary):
			#features_by_article[article_index][sentence_position][-1] = 1
		#else:
			#features_by_article[article_index][sentence_position][-1] = 0

		
		sentence_index += 1


	fuzzy_scores.append(fuzzy_score)
	article_index +=1

	bar_counter += 1
	if(bar_counter % len_bar == 0):
		bar += '='
		end_bar -= 1
		print(bar+' '*end_bar+']', end="\r", flush=True)


File_object = open(r'NN/fuzzy_scores_pickled', 'wb')
pickle.dump(fuzzy_scores, File_object,-1)
File_object.close()

File_object = open(r'Summaries_result_vectors/fuzzy_logic_pickled', 'wb')
pickle.dump(features_by_article, File_object,-1)
File_object.close()

File_object = open(r"NN/naive_bayes_sgm_pickled","wb")
pickle.dump(features, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/fl/thirty_pickle', 'wb')
pickle.dump(thirty_percent, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/fl/forty_pickle', 'wb')
pickle.dump(forty_percent, File_object,-1)
File_object.close()

File_object = open(r'Testing/eval/fl/fifty_pickle', 'wb')
pickle.dump(fifty_percent, File_object,-1)

print(bar+' '*end_bar+'] - COMPLETED', end="\n", flush=True)